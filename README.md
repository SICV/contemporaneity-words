Words
=======

Timed text player based on syncronized transcription output of [Gentle](http://lowerquality.com/gentle/).

Made for the [Contemporary Contemporary](http://contemporaneity.au.dk/conference/) conference, part of the [The Contemporary Condition](http://contemporaneity.au.dk/).

Visible online at: <http://sicv.activearchives.org/contemporaneity/words/>

Todo
=====
* The JSON files could be significanly trimmed (as we're not using the phonetics data).

Building
==========

This should do the trick:

   npm install
   make dist/player.js

Uses webpack.
