const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  entry: './src/player.js',
  output: {
    filename: './dist/player.js'
  },
  plugins: [
   new UglifyJSPlugin()
  ]
}