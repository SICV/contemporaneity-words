var karaoke = require("./karaoke.js"),
    d3 = require("d3"),
    async = require("async"),
    kk = [],
    elts = Array.prototype.slice.call(document.querySelectorAll(".karaoke")),
    playersdiv = document.getElementById("players");

async.each(elts, function (x, callback) {
    var link = x.querySelector("a.gentle"),
        audio = x.querySelector("audio"),
        output = x.querySelector(".output");
    // move the audio to the playersdiv
    playersdiv.appendChild(audio);
    d3.json(link.href, function (err, data) {
        var k = karaoke(audio, data, output);
        kk.push(k);
        callback();
    })   
}, function (err) {
    if (err) {
        console.log("an error occured loading sources");
    } else {
        // console.log("all sources loaded");
        return;
        kk.forEach(function (k) {
            k.play();
        })
    }
});
// d3.json(document.getElementById("gentle").href, function (err, data) {
//     karaoke(audio, data, document.getElementById("output"));
//     audio.play();
// })