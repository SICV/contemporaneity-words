var d3 = require("d3");

function karaoke (audio, titles, elt) {
    // console.log("karaoke", audio, titles, elt);
    var that = {},
        words = titles.words,
        numwords = words.length,
        active_word = null,
        curi = null,
        ct = null,
        $elt = d3.select(elt),
        fontSize = d3.scaleLinear().domain([0, 2.0]).range([6, 100]);

    that.play = function () {
        audio.play();
    }

    $elt.on("click", function () {
        (audio.paused) ? audio.play() : audio.pause();
    });

    function set_active_word (w, i) {
        // console.log("set_active_word", w.word, i);
        elt.innerHTML = w.word;
        active_word = w;
        curi = i;
        // set font size based on interval to next word (more pause == bigger)
        if (i+1 < numwords) {
            var nw = words[i+1],
                interval = nw.start - w.start;
            // console.log("interval", interval);
            $elt.style("font-size", fontSize(interval)+"px");
        }
    }

    var timer = d3.timer(tick);
    // audio.addEventListener("timeupdate", tick);
    
    function tick () {
        ct = audio.currentTime;
        if (curi == null) {
            for (var i=0; i<numwords; i++) {
                // console.log(i, ct, numwords);
                var w = words[i];
                if (ct >= w.start && ct < w.end) {
                    set_active_word(w, i);
                    break;
                }
                if (ct < w.start) { break; }
            }
        } else {
            // Attempt to advance to next active word
            for (var i = curi+1; i<numwords; i++) {
                w = words[i];
                if (ct < w.start) break;
                if (ct >= w.start && ct < w.end) {
                    set_active_word(w, i);
                    break;
                }
            }            
        }
    }
    return that;
}

module.exports = karaoke;